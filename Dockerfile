# Use the official GitLab CE image as a base image
FROM gitlab/gitlab-ce:latest

# Expose necessary ports
EXPOSE 443
EXPOSE 80
EXPOSE 22

# Define named volumes
VOLUME /etc/gitlab
VOLUME /var/log/gitlab
VOLUME /var/opt/gitlab

# Set shared memory size
CMD ["run", "--shm-size=6gb"]