#!/bin/bash
docker run --detach \
--publish 10443:443 --publish 10080:80 --publish 10022:22 \
--name gitlab \
--restart always \
--volume gitlab_config:/etc/gitlab \
--volume gitlab_logs:/var/log/gitlab \
--volume gitlab_data:/var/opt/gitlab \
--shm-size 6gb \
gitlab/gitlab-ce:latest
