# gitlab_docker

## Docker gitlab configuration

```bash
  docker run --detach \
  --publish 10443:443 --publish 10080:80 --publish 10022:22 \
  --name gitlab \
  --restart always \
  --volume gitlab_config:/etc/gitlab \
  --volume gitlab_logs:/var/log/gitlab \
  --volume gitlab_data:/var/opt/gitlab \
  --shm-size 2gb \
  gitlab/gitlab-ce:latest
```

## Logs
CONTAINER_NAME=gitlab

```bash
docker logs -f CONTAINER_ID or CONTAINER_NAME
```

```bash
docker logs -f gitlab
```

## Up

### docker-compose
```bash
docker-compose up -d
```

## Login

First login may be used with:
* username : root
* password : PASSWORD

The PASSWORD is generated and can be obtained using next command

```bash
docker exec -it CONTAINER_NAME grep 'Password:' /etc/gitlab/initial_root_password
```

```bash
docker exec -it gitlab grep 'Password:' /etc/gitlab/initial_root_password
```
